package configs

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

type BproxyConf struct {
	ListenIP          string
	ListenPort        int
	BagentIPs         map[string]struct{}
	BagentIPNets      []*net.IPNet
	BserverIPs        map[string]struct{}
	BserverIPNets     []*net.IPNet
	AllowIPs          map[string]struct{}
	AllowIPNets       []*net.IPNet
	ConnectionTimeout time.Duration
}

var Config BproxyConf

func (c *BproxyConf) Init(confAddr string) error {
	f, err := os.Open(confAddr)
	if err != nil {
		return err
	}
	c.ListenPort = 8092
	c.ConnectionTimeout = 5 * time.Second

	c.BagentIPs = map[string]struct{}{}
	c.BagentIPNets = []*net.IPNet{}
	c.BserverIPs = map[string]struct{}{}
	c.BserverIPNets = []*net.IPNet{}
	c.AllowIPs = map[string]struct{}{}
	c.AllowIPNets = []*net.IPNet{}

	buf := bufio.NewReader(f)
	for {
		l, _, err := buf.ReadLine()

		if err != nil || io.EOF == err {
			if err == io.EOF {
				return nil
			}
			return err
		}
		line := string(l)
		line = strings.TrimSpace(line)
		if strings.HasPrefix(line, "#") {
			continue
		}
		if len(line) == 0 {

			return errors.New("config format error")
		}
		splits := strings.Split(line, "=")
		if len(splits) < 2 {
			log.Println("shutting down")
			return errors.New("config format error")
		}
		if len(splits[0]) == 0 || len(splits[1]) == 0 {
			return errors.New("config format error")
		}
		switch splits[0] {
		case "listen":
			c.ListenIP = splits[1]
		case "bagent_ip":
			for _, split := range strings.Split(splits[1], ",") {
				split = strings.TrimSpace(split)
				if !strings.Contains(split, "/") {
					c.BagentIPs[split] = struct{}{}
					continue
				} else {
					_, ipnet, err := net.ParseCIDR(split)
					if err != nil {
						return err
					}
					c.BagentIPNets = append(c.BagentIPNets, ipnet)
				}
			}

			ips := strings.Split(splits[1], ",")
			for _, ip := range ips {
				c.BagentIPs[ip] = struct{}{}
			}
		case "bserver_ip":
			for _, split := range strings.Split(splits[1], ",") {
				split = strings.TrimSpace(split)
				if !strings.Contains(split, "/") {
					c.BserverIPs[split] = struct{}{}
					continue
				} else {
					_, ipnet, err := net.ParseCIDR(split)
					if err != nil {
						return err
					}
					c.BserverIPNets = append(c.BserverIPNets, ipnet)
				}
			}

			ips := strings.Split(splits[1], ",")
			for _, ip := range ips {
				c.BagentIPs[ip] = struct{}{}
			}
		case "listen_port":
			port, convErr := strconv.Atoi(splits[1])
			if convErr != nil {
				return errors.New("port should be a number")
			}
			c.ListenPort = port
		case "connect_timeout":
			time.ParseDuration(splits[1])
			dur, err := time.ParseDuration(splits[1])
			if err != nil {
				return errors.New("connect_timeout format error")
			}
			c.ConnectionTimeout = dur
		case "allow_ip":
			fmt.Println("allow ", splits[1])
			for _, split := range strings.Split(splits[1], ",") {
				split = strings.TrimSpace(split)
				if !strings.Contains(split, "/") {
					c.AllowIPs[split] = struct{}{}
					fmt.Println("map ", c.AllowIPs)
				} else {
					_, ipnet, err := net.ParseCIDR(split)
					if err != nil {
						return err
					}
					c.AllowIPNets = append(c.AllowIPNets, ipnet)
				}
			}
		}
	}
}
