## commit note 2018.05.10
bproxy 解析收到的请求，拿到目的 bagent 的 ip 和端口号，并重新发起请求；接收到 bagent 的请求时，出于性能考虑，不再解析，直接返回给客户端

- bproxy 框架，配置，restful 服务等
- 支持 bget

e.g. 不通过 proxy 请求 bagent
./bget -i 127.0.0.1 -p 8090 -k today
e.g. 通过 proxy 请求 bagent
./bget -i 127.0.0.1 -p 8090 -x 127.0.0.1 -P 8092 -k today

## commit note 2018.05.15
可以做各个模块的 proxy，不需要单独开发，只需要修改配置
proxy 做权限校验时，会根据来源 IP，去 bserver_ip/agent_ip/allow_ip 依次比对，若命中，则认为可以发起请求。
