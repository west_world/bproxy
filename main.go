package main

import (
	"bproxy/configs"
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var NotAllowedResp = "{\"code\":301,\"msg\":\"IP not allowed\"}"

func handleTunneling(w http.ResponseWriter, r *http.Request) {
	srcIP := strings.Split(r.RemoteAddr, ":")[0]
	if !IsAllowed(srcIP) {
		w.Write([]byte(NotAllowedResp))
		return
	}

	dest_conn, err := net.DialTimeout("tcp", r.Host, 10*time.Second)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	w.WriteHeader(http.StatusOK)
	hijacker, ok := w.(http.Hijacker)
	if !ok {
		http.Error(w, "Hijacking not supported", http.StatusInternalServerError)
		return
	}
	client_conn, _, err := hijacker.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
	}
	go transfer(dest_conn, client_conn)
	go transfer(client_conn, dest_conn)
}

func transfer(destination io.WriteCloser, source io.ReadCloser) {
	defer destination.Close()
	defer source.Close()
	io.Copy(destination, source)
}

func handleHTTP(w http.ResponseWriter, req *http.Request) {
	srcIP := strings.Split(req.RemoteAddr, ":")[0]
	if !IsAllowed(srcIP) {
		w.Write([]byte(NotAllowedResp))
		return
	}

	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	defer resp.Body.Close()
	copyHeader(w.Header(), resp.Header)
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func IsAllowed(IP string) bool {
	if _, ok := configs.Config.AllowIPs[IP]; ok {
		return true
	}
	for _, IPNet := range configs.Config.AllowIPNets {
		if IPNet.Contains(net.ParseIP(IP)) {
			return true
		}
	}
	if _, ok := configs.Config.BagentIPs[IP]; ok {
		return true
	}
	for _, IPNet := range configs.Config.BagentIPNets {
		if IPNet.Contains(net.ParseIP(IP)) {
			return true
		}
	}
	if _, ok := configs.Config.BserverIPs[IP]; ok {
		return true
	}
	for _, IPNet := range configs.Config.BserverIPNets {
		if IPNet.Contains(net.ParseIP(IP)) {
			return true
		}
	}
	return false
}

func main() {
	fmt.Println("bproxy starting...")

	var confAddr string
	// 可以指定各个配置文件的地址，若不指定则默认在 /opt/bagent 下
	flag.StringVar(&confAddr, "c", "/opt/bagent/bproxy.cnf", "bproxy config file")

	flag.Parse()

	configs.Config = configs.BproxyConf{}

	err := configs.Config.Init(confAddr)
	if err != nil {
		os.Exit(1)
	}

	server := &http.Server{
		Addr: configs.Config.ListenIP + ":" + strconv.Itoa(configs.Config.ListenPort),
		Handler: http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.Method == http.MethodConnect {
				handleTunneling(w, r)
			} else {
				handleHTTP(w, r)
			}
		}),
		// Disable HTTP/2.
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
	}
	log.Fatal(server.ListenAndServe())
}
